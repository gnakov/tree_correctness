import .tree

namespace tree

variables {α : Type} [decidable_linear_order α]

def inorder : tree α → list α
| leaf := []
| (node l a r) := inorder l ++ [a] ++ inorder r

def insert (x : α) : tree α → tree α 
| leaf            := node leaf x leaf
| t@(node l a r)  := if (x < a) then node (insert l) a r
                     else if (x = a) then t
                     else node l a $ insert r


-- returns the leftmost value and the tree with the corresponding node X deleted
-- the right subtree of X (if exists) is conjoined to the parent of X
def split_min : tree α → α → tree α  → α × (tree α)
| leaf a r := (a,r)
| (node ll' la' lr') a r := 
    have res: _ := split_min ll' la' lr',
        (res.fst, node res.snd a r)

def delete (x : α ) : tree α → tree α
| leaf         := leaf
| (node l a r) := 
    if (x < a) then node (delete l) a r
    else if (x = a) then 
    match r with
        | leaf := l
        | node rl ra rr  := 
            let ⟨a', r'⟩ := split_min rl ra rr
            in node l a' r'
    end
    else node l a (delete r)

def mem (x : α) : tree α → Prop
| leaf := false
| (node l a r) := if (x < a) then mem l
                  else if (x = a) then true
                  else mem r

end tree