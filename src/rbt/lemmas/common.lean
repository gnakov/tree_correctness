import ..operations
import ..tactic.cases_rbt
import ...list_operations

namespace rbt
namespace lemmas

variables {α : Type} [decidable_linear_order α]
variables {c : color}
variables {t l r : rbt α}
variables {a x : α}

section inorder

lemma inorder_paint: inorder (paint c t) = inorder t :=
by induction t; simp!

lemma inorder_baliL: inorder (baliL l a r) = inorder l ++ a :: inorder r :=
by cases_rbt l l_l l_r; simp!

lemma inorder_baliR: inorder (baliR l a r) = inorder l ++ a :: inorder r :=
by cases_rbt r r_l r_r; simp!

lemma inorder_baldL: inorder (baldL l a r) = inorder l ++ a :: inorder r :=
by cases_rbt l r r_l; simp! [inorder_baliL, inorder_baliR, inorder_paint]

lemma inorder_baldR: inorder (baldR l a r) = inorder l ++ a :: inorder r :=
by cases_rbt l r l_r; simp! [inorder_baliL, inorder_baliR, inorder_paint]

lemma combine_eq_leaf : combine l r = leaf → l = leaf ∧ r = leaf := 
begin
  intro h,
  cases_rbt l r;
  simp [combine] at h ⊢; try {contradiction};
  generalize_hyp hc : combine l_r r_l = clr at h;

  { cases_rbt clr; simp [combine] at h;

    { replace h := congr_arg inorder h,
      simp [inorder_baldL, inorder] at h, 
      contradiction } 
    <|> finish
  }
end

lemma inorder_combine: inorder (combine l r) = inorder l ++ inorder r := 
begin
  induction l generalizing r,
  { cases_rbt r; simp [combine, inorder] },

  induction r; cases_matching* [color];
  simp [combine, inorder],
  { 
    generalize hc : combine l_r r_l = clr,
    cases_rbt clr; simp [combine, inorder],
    {
      replace hc := combine_eq_leaf hc,
      cases hc, subst_vars,
      simp [inorder]
    },
    all_goals {
      replace hc := congr_arg inorder hc,
      simp [inorder] at hc,
      replace hc := eq.trans l_ih_r.symm hc,
      simp [utils.cancel_append],
      rw [←list.append_assoc],
      simp [hc]
    }
  },
  {
    specialize @l_ih_r (node r_l r_a black r_r),
    simp [l_ih_r, inorder]
  },
  {
    simp [r_ih_l, inorder]
  },
  {
    generalize hc : combine l_r r_l = clr,
    cases_rbt clr; simp [combine, inorder],
    {
      replace hc := combine_eq_leaf hc,
      simp [inorder_baldL, inorder, hc]
    },
    all_goals {
      replace hc := congr_arg inorder hc,
      simp [inorder] at hc,
      replace hc := eq.trans l_ih_r.symm hc,
      simp [utils.cancel_append, inorder, inorder_baldL],
      rw [←list.append_assoc],
      simp [hc]
    }
  }
end

end inorder
---------------------------------------------------------------------------------------------------
section invariants

lemma inv_empty : inv (leaf : rbt α) := 
by simp [inv, invc, invh, get_color]

lemma invc2I: invc t → invc2 t := 
by cases_rbt t; finish [invc, invc2]

lemma color_paint_black: get_color (paint black t) = black :=
by cases t; finish [paint, get_color]

lemma paint_invc2: invc2 t → invc2 (paint c t) :=
by cases t; finish [paint, invc2]

lemma invc_paint_black: invc2 t → invc (paint black t) :=
by cases t; finish [invc2, invc, paint]

lemma invh_paint: invh t → invh (paint c t) :=
by cases t; finish [invh, paint]

lemma invc_baliL: invc2 l → invc r  → invc (baliL l a r) :=
by cases_rbt l l_l l_r; finish [invc2, invc, baliL]

lemma invc_baliR: invc l → invc2 r → invc (baliR l a r) :=
by cases_rbt r r_l r_r; finish [invc2, invc, baliR]

lemma bheight_baliL:
  bheight l = bheight r → bheight (baliL l a r) = 1 + bheight l :=
by cases_rbt l l_l l_r; finish [bheight, baliL]

lemma bheight_baliR:
  bheight l = bheight r → bheight (baliR l a r) = 1 + bheight l :=
by cases_rbt r r_l r_r; finish [bheight, baliR]

lemma invh_baliL: 
   invh l → invh r → bheight l = bheight r → invh (baliL l a r) :=
by cases_rbt l l_l l_r; finish [bheight, invh, baliL]

lemma invh_baliR: 
  invh l → invh r → bheight l = bheight r → invh (baliR l a r) :=
by cases_rbt r r_l r_r; finish [bheight, invh, baliR]

lemma bheight_paint_red: 
  get_color t = black → bheight (paint red t) = bheight t - 1 :=
by cases t; finish [bheight, paint, get_color]

lemma invh_baldL_invc:
   invh l → invh r → bheight l + 1 = bheight r → invc r → 
   invh (baldL l a r) ∧ bheight (baldL l a r) = bheight l + 1 :=
begin
  intros vhl vhr bh vcr,
  cases_rbt l r r_l; simp [bheight, invh, baldL] at ⊢ bh vhr vhl;
  unfold1 invc at vcr; try {simp [get_color] at vcr},
  repeat {apply and.intro},

  all_goals { cases_matching* [_ ∧ _]},
  all_goals { try { contradiction } },
  all_goals {
    try { apply invh_baliR },
    try { apply bheight_baliR },
  },
  all_goals {
    try { simp [invh] },
    try { simp [bheight] },
    try { apply invh_paint }
  },
  assumption',
  
  all_goals {
    try {rw [bheight_baliR _]};
    try {rw [bheight_paint_red vcr_right_right]};
    try {simp [bheight]}
  },

  all_goals {
    try { replace bh := eq.symm (transferK_to_right bh.symm), simp at bh },
    safe
  },

  all_goals {
    replace bh := eq.symm (one_eq_one_plus bh.symm),
    simp [not_k_plus_one_eq_k] at bh, 
    contradiction
  }

end

lemma invh_baldL_black: 
  invh l → invh r → bheight l + 1 = bheight r → get_color r = black  → 
  invh (baldL l a r) ∧ bheight (baldL l a r) = bheight r := 
begin
  intros vhl vhr bh cr,
  cases_rbt l r r_l; simp [bheight, invh, baldL] at ⊢ bh vhr vhl;
  repeat {apply and.intro},
  all_goals { cases_matching* [_ ∧ _]},
  all_goals { try { contradiction } },
  assumption',
  all_goals {
    try {apply invh_baliR};
    try {rw [bheight_baliR _]};
    try {simp [invh, bheight]}
  },
  assumption',
  all_goals {
    try {replace bh := one_eq_one_plus bh, assumption},
    try {simpa [not_k_plus_one_eq_k] using bh},
  },
  all_goals { finish }
end

lemma invc_baldL: invc2 l → invc r → get_color r = black → invc (baldL l a r) :=
begin
  intros vc2l vcr cr,
  cases_rbt l r r_l; simp [invc2, invc, baldL, get_color] at ⊢ vc2l vcr cr;
  all_goals { cases_matching* [_ ∧ _]},
  all_goals { try {contradiction} },
  all_goals { 
    try {apply invc_baliR};
    try {simp [invc, invc2]}
  },
  all_goals {finish}
end

lemma invc2_baldL: invc2 l → invc r → invc2 (baldL l a r) := 
begin
  intros vc2l vcr,
  cases_rbt l r r_l; simp [invc2, invc, baldL, get_color] at ⊢ vc2l vcr,
  all_goals { cases_matching* [_ ∧ _]},
  all_goals { try {contradiction} },
  all_goals {
    try {apply and.intro};
    try {apply invc_baliR};
    try {apply paint_invc2};
    try {apply invc2I};
    try {apply invc_baliR};
    try {simp [invc, invc2]},
  },
  all_goals {finish}
end

lemma invh_baldR_invc:
  invh l → invh r → bheight l = bheight r + 1 → invc l → 
  invh (baldR l a r) ∧ bheight (baldR l a r) = bheight l :=
-- completely analogous to invh_baldL_invc
begin
  intros vhl vhr bh vcr,
  cases_rbt l r l_r; simp [bheight, invh, baldR] at ⊢ bh vhr vhl;
  unfold1 invc at vcr; try {simp [get_color] at vcr},
  repeat {apply and.intro},

  all_goals { cases_matching* [_ ∧ _]},
  all_goals { try { contradiction } },
  all_goals {
    try { apply invh_baliL },
    try { apply bheight_baliL },
  },
  all_goals {
    try { simp [invh] },
    try { simp [bheight] },
    try { apply invh_paint }
  },
  assumption',
  
  all_goals {
    try {rw [bheight_baliL _]};
    try {rw [bheight_paint_red vcr_right_right]},
  },

  all_goals {
    try { replace bh := eq.symm (transferK_to_right bh.symm), simp at bh },
    safe,
    try { replace bh := eq.symm (one_eq_one_plus bh.symm) },
    try { simp [not_k_plus_one_eq_k] at bh, contradiction }
  },

  all_goals {
    try { exact a_1 bh },
    try { exact a_1 bh.symm }
  }
  
end


lemma invc_baldR: invc l → invc2 r  → get_color l = black → invc (baldR l x r) :=
-- completely analogous to invc_baldL
begin
  intros vc2l vcr cr,
  cases_rbt l r l_r; simp [invc2, invc, baldR, get_color] at ⊢ vc2l vcr cr;
  all_goals { cases_matching* [_ ∧ _]},
  all_goals { try {contradiction} },
  all_goals { 
    try {apply invc_baliL};
    try {simp [invc, invc2]}
  },
  all_goals {finish}
end

lemma invc2_baldR: invc l → invc2 r → invc2 (baldR l x r) :=
-- completely analogous to invc2_baldL
begin
  intros vc2l vcr,
  cases_rbt l r l_r; simp [invc2, invc, baldR, get_color] at ⊢ vc2l vcr,
  all_goals { cases_matching* [_ ∧ _]},
  all_goals { try {contradiction} },
  all_goals {
    try {apply and.intro};
    try {apply invc_baliL};
    try {apply paint_invc2};
    try {apply invc2I};
    try {apply invc_baliL};
    try {simp [invc, invc2]},
  },
  all_goals {try {finish}},
end

lemma invh_combine:
  invh l → invh r → bheight l = bheight r →
  invh (combine l r) ∧ bheight (combine l r) = bheight l :=
begin
  intros vhl vhr bh,
  induction l generalizing r,
  { cases_rbt r; finish [invh, combine] },

  induction r; cases_matching* [color];
  simp [invh, combine, bheight],
  { finish [bheight, invh] },
  { finish [bheight, invh] },
  {
    generalize hc : combine l_r r_l = clr,
    cases_rbt clr; simp [combine];
    simp [invh, bheight] at ⊢ bh vhl vhr,
    {
      replace hc := combine_eq_leaf hc,
      cases hc, subst_vars,
      finish [bheight]
    },
    all_goals {
      have invh_comb    := congr_arg invh hc,
      have bheight_comb := congr_arg bheight hc,
      simp [invh, bheight] at invh_comb bheight_comb bh,
      cases_matching* [_ ∧ _],
      specialize l_ih_r vhl_right_left vhr_left 
                (eq.trans bh.symm vhl_right_right).symm,
      rw [bheight_comb, invh_comb] at l_ih_r,
      finish
    }
  },
  {
    simp [invh, bheight] at vhl bh,
    specialize l_ih_r vhl.right.left vhr,
    rw vhl.right.right at bh,
    simp [bheight] at l_ih_r,
    specialize l_ih_r bh,
    finish
  },
  {
    simp [invh, bheight] at vhl vhr bh r_ih_l,
    specialize r_ih_l vhr.left bh,
    finish
  },
  {
    generalize hc : combine l_r r_l = clr,
    cases_rbt clr; simp [combine];
    simp [invh, bheight] at ⊢ bh vhl,
    {
      replace hc := combine_eq_leaf hc,
      cases hc, subst_vars,
      simp [bheight] at bh,
      have h := @invh_baldL_black _ _ _ _ l_a vhl.left vhr 
                (by rw bh; unfold bheight) (by simp [get_color]),
      simp [bheight] at h,
      rw bh, simpa
    },
    {
      have invh_comb    := congr_arg invh hc,
      have bheight_comb := congr_arg bheight hc,
      simp [invh, bheight] at invh_comb bheight_comb bh vhr,
      cases_matching* [_ ∧ _],
      specialize l_ih_r vhl_right_left vhr_left
                (eq.trans bh.symm vhl_right_right).symm,
      rw [bheight_comb, invh_comb] at l_ih_r,
      rw ←vhl_right_right at l_ih_r,
      finish
    },
    {
      have invh_comb    := congr_arg invh hc,
      have bheight_comb := congr_arg bheight hc,
      simp [invh, bheight] at invh_comb bheight_comb bh vhr,
      cases_matching* [_ ∧ _],
      specialize l_ih_r vhl_right_left vhr_left
                (eq.trans bh.symm vhl_right_right).symm,
      rw [bheight_comb, invh_comb] at l_ih_r,
      rw ←vhl_right_right at l_ih_r,

      have h := @invh_baldL_black _ _ _ 
                (B (node clr_l clr_a black clr_r) r_a r_r)
                l_a 
                vhl_left
                (by simp [invh, bheight];
                    rw ←vhr_right_right; rw ←bh; finish)
                (by simp [bheight]; finish)
                (by simp [get_color]),
      simp [bheight] at h,
      rwa l_ih_r.right at h
    }
  }
end

lemma invc_combine:
  invc l → invc r → 
  (get_color l = black → get_color r = black → invc (combine l r)) ∧ invc2 (combine l r) := 
-- look at the proof of invh_combine and use `invc_baldL` and `invc2I`
begin
  intros vcl vcr,
  induction l generalizing r,
  { cases_rbt r; finish [get_color, combine, invc2, invc]},

  induction r; cases_matching* [color];
  simp [invc, invc2, invh, combine, bheight],
  { finish [invc] },
  { finish [invc] },
  {
    generalize hc : combine l_r r_l = clr,
    cases_rbt clr; simp [combine];
    simp [invc2, get_color] at ⊢ vcl vcr;
    unfold1 invc at ⊢ vcl vcr,
    {
      replace hc := combine_eq_leaf hc,
      finish
    },
    all_goals {
      have invh_comb    := congr_arg invc hc,
      unfold1 invc at invh_comb,
      simp at vcr vcl ⊢,
      cases_matching* [_ ∧ _],
      specialize l_ih_r vcl_right_left vcr_left,
      replace l_ih_r := l_ih_r.left vcl_right_right_right vcr_right_right_left,
      rw invh_comb at l_ih_r,
      finish
    }
  },
  {
    unfold1 invc at vcl, simp at vcl,
    specialize l_ih_r vcl.right.left vcr,
    replace l_ih_r := l_ih_r.left vcl.right.right.right,
    finish [get_color]
  },
  {
    unfold1 invc at vcr, simp at vcr,
    specialize r_ih_l vcr.left,
    finish [get_color]
  },
  {
    generalize hc : combine l_r r_l = clr,
    cases_rbt clr; simp [combine];
    simp [get_color, invc] at ⊢ vcl,
    {
      replace hc := combine_eq_leaf hc,
      cases hc, subst_vars,
      have h := @invc_baldL _ _ _ _ l_a
                (invc2I vcl.left) vcr (by simp [get_color]),
      finish [invc2I]
    },
    {
      have invc2_comb    := congr_arg invc2 hc,
      simp [invc2, invc] at invc2_comb vcr ⊢,
      cases_matching* [_ ∧ _],
      specialize l_ih_r vcl_right vcr_left,
      replace l_ih_r := l_ih_r.right,
      finish
    },
    {
      have invc2_comb    := congr_arg invc2 hc,
      simp [invc2, invc] at invc2_comb vcr ⊢,
      cases_matching* [_ ∧ _],
      specialize l_ih_r vcl_right vcr_left,
      replace l_ih_r := l_ih_r.right,

      have h := @invc_baldL _ _ _ (B (node clr_l clr_a black clr_r) r_a r_r) l_a
                (invc2I vcl_left)
                (by finish [invc])
                (by simp [get_color]),
      exact ⟨h, invc2I h⟩
    }
  }
end

end invariants

end lemmas
end rbt