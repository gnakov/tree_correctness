import .common

namespace rbt
namespace lemmas

variables {α : Type} [decidable_linear_order α]
variable  {t : rbt α}
variable  {x : α}


private lemma inorder_del: 
  sorted (inorder t) →  inorder (del x t) = del_list x (inorder t) :=
begin
  intro h,
  induction t,
  { simp! },
  { simp [inorder, del] at ⊢ h,
    split_ifs,
    {
      generalize dh : (del x t_l) = l,
      cases_rbt t_l; simp [del],
      { simp [del] at dh, rw ←dh, simp [del_list_sorted2 h h_1], simp! },
      all_goals {
        simp [inorder], rw ←dh, 
        repeat { rw [←list.cons_append, ←list.append_assoc]},
        rw [del_list_sorted3 h h_1],
        specialize t_ih_l (sorted_wrt_append (sorted_mid_iff.mp h).left),
        try {simp [inorder_baldL] },
        simp [t_ih_l, inorder]
      }
    },
    { 
      simp [del_list_sorted1 h (le_of_not_lt h_1), inorder_combine],
      simp [h_2, del_list]
    },
    {
      generalize dh : (del x t_r) = r,
      cases_rbt t_r; simp [del],
      { simp [del] at dh, rw ←dh, 
        simp [del_list_sorted1 h (le_of_not_lt h_1)],
        simp [inorder, del_list, h_2],
      },
      all_goals {
        rw ←dh, 
        rw [del_list_sorted1 h (le_of_not_lt h_1)],
        specialize t_ih_r (sorted_wrt_cons (sorted_mid_iff.mp h).right),
        try {simp [inorder_baldR] },
        simp [t_ih_r, inorder, del_list, h_2],
      }
    }
  }
end

theorem inorder_delete:
  sorted (inorder t) → inorder (delete x t) = del_list x (inorder t) :=
by simp [delete, inorder_paint]; exact inorder_del
---------------------------------------------------------------------------------------------------
section invariants

private lemma del_invc_invh: invh t → invc t → invh (del x t) ∧
    (get_color t = red ∧ bheight (del x t) = bheight t ∧ invc (del x t) ∨
     get_color t = black ∧ bheight (del x t) = bheight t - 1 ∧ invc2 (del x t)) :=
begin
  intros vht vct,
  induction t,
  { simp! },
  { simp [del],
    split_ifs,
    {
       generalize dh : (del x t_l) = l,
       cases_rbt t_l; 
       simp [del, invh, bheight, get_color, invc, invc2] at vct vht ⊢;
       try {specialize t_ih_l vht.left (by simpa [invc] using vct.left)},
       { simp [del] at dh, rw ←dh,
         cases t_c; finish [del, invh, invc, invc2],
       },
       {
         rw ←dh,
         cases t_c; finish [invh, invc, get_color, bheight, del, invc2],
       },
       {
         cases t_c; {
          simp [get_color, bheight] at t_ih_l ⊢ vct, 
          have h₁ : invh (baldL l t_a t_r) ∧ bheight (baldL l t_a t_r) = bheight l + 1 :=
            by apply invh_baldL_invc; finish,
          try { have h₂ : invc (baldL l t_a t_r)  := by apply invc_baldL; finish},
          try { have h₂ : invc2 (baldL l t_a t_r) := by apply invc2_baldL; finish},
          rw ← dh at h₁ {occs := occurrences.pos [3]},
          rw t_ih_l.right.left at h₁,
          finish
         }
       }
    },
    { 
      cases t_c; 
      simp [get_color]; simp [invh, invc, bheight, invc2, get_color] at vht vct ⊢;
      { have h₁ : invh (combine t_l t_r) ∧ bheight (combine t_l t_r) = bheight t_l :=
          by apply invh_combine; finish,
        try { have h₂ : invc (combine t_l t_r) := by apply (invc_combine _ _).left; finish },
        try { have h₂ : invc2 (combine t_l t_r) := by apply (invc_combine _ _).right; finish },
        finish
      }
    },
    { 
      generalize dh : (del x t_r) = r,
      cases_rbt t_r; 
      simp [del, invh, bheight, get_color, invc, invc2] at vct vht ⊢;
      try {specialize t_ih_r vht.right.left (by simpa [invc] using vct.right.left)},
      { simp [del] at dh, rw ←dh,
        cases t_c;  finish [del, invh, invc, bheight],
      },
      {
        rw ←dh,
        cases t_c; finish [invh, invc, get_color, bheight, del, invc2]
      },
      {
        cases t_c; {
         simp [get_color, bheight] at t_ih_r ⊢ vct, 
         have h₁ : invh (baldR t_l t_a r) ∧ bheight (baldR t_l t_a r) = bheight t_l:=
           by apply invh_baldR_invc; finish,
         try { have h₂ : invc (baldR t_l t_a r)  := by apply invc_baldR; finish},
         try { have h₂ : invc2 (baldR t_l t_a r) := by apply invc2_baldR; finish},
         finish
        }
      }
    }
  }
end

theorem invar_delete: inv t → inv (delete x t) :=
begin
  simp [inv, delete, color_paint_black],
  intros vct vht cb,
  have h := @del_invc_invh _ _ _ x vht vct,
  simp [cb] at h,
  finish [invc_paint_black, invh_paint]
end

end invariants

end lemmas
end rbt