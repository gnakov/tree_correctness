import tactic

namespace tactic
namespace interactive

open interactive
open lean.parser

meta def with_l_val_r := 
(tk "with" *> do 
  left_tree ← types.ident_,
  val ← types.ident_ <|> return `_,
  right_tree ← types.ident_ <|> return `_,
  return $ (left_tree, val, right_tree)) 
<|> return (`_, `_, `_)


meta def cases_rbt : parse (many ident) → parse with_l_val_r → tactic unit 
| [] hc := return ()
| (ca :: cas) hn :=
  try $ do
    x ← get_unused_name $ "__col",
    pca ← resolve_name ca,
    cases (none, pca) [hn.fst, hn.snd.fst, x, hn.snd.snd],
    swap,
    px ← resolve_name x,
    cases (none, px) [],
    swap 3,
    all_goals $ cases_rbt cas hn

end interactive
end tactic