import ..set_specs
import .operations
import .lemmas

namespace rbt
open lemmas

variables {α : Type} [decidable_linear_order α]

instance : set_by_ordered rbt α :=
{
  emptyc  := leaf,
  mem     := mem,
  insert  := insert,
  delete  := delete,
  invar   := inv,
  inorder := inorder
}

instance : is_lawful_set_by_ordered rbt α :=
{
  inorder_empty       := rfl,
  isin                := λ _ _ ⟨_, h⟩, isin h,
  inorder_insert      := λ _ _ ⟨_, h⟩, inorder_insert h,
  inorder_delete      := λ _ _ ⟨_, h⟩, inorder_delete h,
  inorder_inv_empty   := inv_empty,
  inorder_inv_insert  := λ _ _ ⟨h,_⟩, invar_insert h,
  inorder_inv_delete  := λ _ _ ⟨h,_⟩, invar_delete h
}

end rbt