import .common

namespace rbt
namespace lemmas

variables {α : Type} [decidable_linear_order α]
variable  {t : rbt α}
variable  {x : α}

private lemma inorder_ins:
  sorted (inorder t) → inorder (ins x t) = ins_list x (inorder t) :=
begin
  intro ht,
  induction t,
  { simp [ins, inorder, ins_list] },

  { cases t_l, 
    { -- we are inserting in a tree with empty left subtree
      simp [inorder, ins, ins_list],
      -- generate 6 cases, all of these are simple and do not any IH, 
      -- so solve them simultaneously
      split_ifs; cases t_c;
      -- the cases are:
      -- 1) (x < t_a) inorder (ins x (node leaf t_a Black t_r)) = x :: t_a :: inorder t_r
      -- 2) (x < t_a) inorder (ins x (node leaf t_a Red t_r))   = x :: t_a :: inorder t_r
      -- 3) (x = t_a) inorder (ins x (node leaf t_a Black t_r)) = t_a :: inorder t_r
      -- 4) (x = t_a) inorder (ins x (node leaf t_a Red t_r))   = t_a :: inorder t_r
      -- 5) (x > t_a) inorder (ins x (node leaf t_a Black t_r)) = t_a :: ins_list x (inorder t_r)))
      -- 6) (x > t_a) inorder (ins x (node leaf t_a Red t_r))   = t_a :: ins_list x (inorder t_r)))
      try { simp [inorder, ins, ins_list] };
      try { simp [h, h_1, inorder] at ht ⊢};
      try { finish };
      try { simp [inorder_baliR, inorder] };
      solve_by_elim with sorted_lems
    },
    
    cases t_c; {  
      simp [ins],
      split_ifs; 
      try {simp [inorder, ins, ins_list] {contextual := tt}};
      try { simp [h, h_1, inorder] at ht,
      rw [←list.cons_append, ←list.append_assoc] at ht};
      try {simp [inorder] at t_ih_l},

      {
        have sorted_tl : sorted (inorder t_l_l ++ t_l_a :: inorder t_l_r ++ [t_a]) := 
          (sorted_mid_iff.mp ht).left,
        try {simp [inorder_baliL]},
        rw [t_ih_l (sorted_wrt_append sorted_tl)],
        rw [←list.cons_append, ←list.append_assoc],
        rw [ins_list_sorted2 _ sorted_tl h]
      },

      {
        have sorted_tl : sorted (inorder t_l_l ++ t_l_a :: (inorder t_l_r ++ [t_a])) := 
          by rw [←list.cons_append, ←list.append_assoc]; exact (sorted_mid_iff.mp ht).left,
            
        have sorted_tl₁ : sorted (t_l_a :: inorder t_l_r ++ [t_a]) := 
            (sorted_mid_iff.mp sorted_tl).right,
            
        have sorted_tl₂ : sorted (inorder t_l_l ++ [t_l_a]) := 
            (sorted_mid_iff.mp sorted_tl).left,

        have : t_l_a < t_a := (sorted_mid_iff2.mp sorted_tl₁).right.left ,
        have : t_l_a ≤ t_a := le_of_lt this,
        have : t_a ≠ t_l_a := (ne_of_lt ‹ t_l_a < t_a ›).symm,

        cases t_l_c;
        {
          subst_vars,
          rw [ins_list_sorted1 _  sorted_tl₂ ‹ t_l_a ≤ t_a › ],
          simp! [lt_asymm ‹ t_l_a < t_a ›, ‹ t_a ≠ t_l_a ›],
          rw [ins_list_sorted1 _ (sorted_wrt_cons sorted_tl₁) (le_refl t_a)],
          simp! [lt_irrefl t_a],
        }
      },

      {
        have sorted_tl : sorted (t_l_a :: inorder t_l_r ++ t_a :: inorder t_r) := 
          by simp at ht; exact (sorted_mid_iff.mp ht).right,
            
        have sorted_tl₁ : sorted (inorder t_l_l ++ [t_l_a]) := 
          by simp at ht; exact (sorted_mid_iff.mp ht).left,
            
        have sorted_tl₂  : sorted (inorder t_l_r ++ [t_a]) := 
          (sorted_mid_iff2.mp sorted_tl).right.right.left,

        have sorted_tl₃ : sorted (inorder t_r) :=
          sorted_wrt_cons (sorted_mid_iff2.mp sorted_tl).right.right.right,
              
        have ineq : ¬x = t_l_a ∧ ¬x < t_l_a := sorted_mk_ineq sorted_tl h,
        have : t_l_a ≤ x  := le_of_not_lt ineq.right,
        have : t_a ≤ x    := le_of_not_lt h,
          
        rw [ins_list_sorted1 _  sorted_tl₁ ‹ t_l_a ≤ x ›],
        simp [ins_list, ineq],
        rw [ins_list_sorted1 _  sorted_tl₂ ‹ t_a ≤ x ›],
        simp [ins_list, h, h_1],
        try { simp [inorder_baliR, inorder] },
        rw [t_ih_r sorted_tl₃]
      }
    }
  }
end

theorem inorder_insert: 
  sorted (inorder t) → inorder (insert x t) = ins_list x (inorder t) :=
by simp [insert, inorder_paint]; exact inorder_ins
---------------------------------------------------------------------------------------------------
section invariants

private lemma invc_ins: invc t → (get_color t = black → invc (ins x t)) ∧ invc2 (ins x t) := 
begin
  induction t; 
  try {cases_matching [color]};
  simp [get_color, invc, invc2, ins],
  
  { intros, split_ifs; simp [invc2]; finish },

  { intros, split_ifs,
    { refine ⟨invc_baliL _ _, invc2I (invc_baliL _ _)⟩; finish },
    { simp [invc, invc2], finish },
    { refine  ⟨ invc_baliR _ _, invc2I (invc_baliR _ _)⟩; finish }
  }
end

private lemma invh_ins: invh t →  invh (ins x t) ∧ bheight (ins x t) = bheight t :=
begin
  induction t;
  try {cases_matching [color]};
  simp [invh, ins, bheight],

  { intros, split_ifs; finish [invh, bheight] },

  {
    intros, split_ifs,
    { rw [←(t_ih_l a).right], refine ⟨ invh_baliL _ _ _, bheight_baliL _ ⟩; finish },
    { finish [invh, bheight] },
    { refine ⟨ invh_baliR _ _ _, bheight_baliR _ ⟩; finish }
  }
end 

theorem invar_insert : inv t → inv (insert x t) :=
by  simp [inv, insert, color_paint_black];
    finish [invc_paint_black, invh_paint, invc_ins, invh_paint, invh_ins]


end invariants

end lemmas
end rbt
