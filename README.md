# (Not Automatic) Functional Correctness Proofs for Functional Search Trees. Implementation in Lean

Based on [Automatic Functional Correctness Proofs for Functional Search Trees](https://www21.in.tum.de/~nipkow/pubs/itp16.html), Tobias Nipkow in Interactive Theorem Proving (ITP 2016).