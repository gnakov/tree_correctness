import ..set_specs
import .operations
import .lemmas

namespace tree
open lemmas

variables {α : Type} [decidable_linear_order α]

instance : set_by_ordered tree α :=
{
  emptyc  := leaf,
  mem     := mem,
  insert  := insert,
  delete  := delete,
  invar   := λ {_}, true,
  inorder := inorder
}


instance : is_lawful_set_by_ordered tree α :=
{
  inorder_empty       := rfl,
  isin                := λ _ _ ⟨_, h⟩, isin h,
  inorder_insert      := λ _ _ ⟨_, h⟩, inorder_insert h,
  inorder_delete      := λ _ _ ⟨_, h⟩, inorder_delete h,
  inorder_inv_empty   := trivial,
  inorder_inv_insert  := by intros; trivial,
  inorder_inv_delete  := by intros; trivial
}

end tree