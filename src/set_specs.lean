import .sorted
import .list_operations

variables (container : Type → Type) (α : Type) [decidable_linear_order α]

class set_by_ordered : Type :=
( emptyc : container α )
( mem : α → container α → Prop)
( insert : α → container α → container α )
( delete : α → container α → container α )
( invar : container α → Prop)
( inorder : container α → list α )

section lawful

open set_by_ordered

class is_lawful_set_by_ordered [set_by_ordered container α] : Prop :=
(inorder_empty : inorder (emptyc container α) =  [])

(isin : ∀ { x : α } {t : container α}, invar t ∧ sorted(inorder t) → 
  (mem x t ↔ (x ∈ inorder t)))

(inorder_insert : ∀ { x : α } {t : container α}, invar t ∧ sorted (inorder t) → 
  inorder (insert x t) = ins_list x (inorder t))

(inorder_delete : ∀ { x : α } {t : container α}, invar t ∧ sorted (inorder t) → 
  inorder(delete x t) = del_list x (inorder t))

(inorder_inv_empty : invar (emptyc container α))

(inorder_inv_insert : ∀ { x : α } {t : container α},  invar t ∧ sorted (inorder t) → 
  invar (insert x t))

(inorder_inv_delete : ∀ { x : α } {t : container α}, invar t ∧ sorted (inorder t) → 
  invar (delete x t))

end lawful