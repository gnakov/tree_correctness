import .sorted
import .utils

open utils

section list_operations

variables {α : Type} [decidable_linear_order α]
variables (x a: α)
variables (xs ys: list α)


def ins_list (a : α): list α → list α 
| []        := [a]
| l@(x::xs) := if (a < x) then a :: l else if (a = x) then l else x :: ins_list xs

def del_list (a : α) : list α → list α 
| []      := []
| (x::xs) := if a = x then xs else x :: del_list xs

end list_operations


namespace simp_atrr

@[user_attribute]
meta def ins_list_simps : user_attribute := { name := `ins_list_simps, descr := "All the lemmas for 'ins_list'" }

@[user_attribute]
meta def del_list_simps : user_attribute := { name := `del_list_simps, descr := "All the lemmas for 'del_list'" }


end simp_atrr

section insert_lemmas


variables {α : Type} [decidable_linear_order α]
variables {x a: α}
variables {xs ys: list α}

lemma a_in_ins_list : a ∈ ins_list x xs → a = x ∨ a ∈ xs :=
assume a_in,
match xs, a_in with
| [], a_in'  := by clear_finish [ins_list]
| (hd :: tl), a_in' := trichotomy_on x hd
  (assume h_ineq, by clear_finish [h_ineq, ins_list])
  (assume h_ineq, by clear_finish [h_ineq, ins_list]) 
  (assume h_ineq, 
    begin
      simp [h_ineq, ins_list] at a_in' ⊢,
      cases a_in',
      case or.inl {clear_finish},
      case or.inr {cases _match tl a_in'; clear_finish}
    end)
end

lemma sorted_ins_list: sorted xs → sorted (ins_list x xs) :=
assume h_xs,
match xs, h_xs with
| [], _          := csingleton 
| (hd :: tl), l  := trichotomy_on x hd
  (assume h_ineq, by simp [h_ineq, ins_list]; exact ccons h_ineq l)
  (assume h_ineq, by simpa [h_ineq, ins_list])
  (assume h_ineq, 
    begin 
      simp [h_ineq, ins_list],
      apply sorted_cons_iff.mpr,
      apply and.intro,
      { intros, cases a_in_ins_list H; clear_finish [sorted_cons_iff.mp l]},
      { solve_by_elim with sorted_lems}
    end)
end

lemma ins_list_sorted: sorted (xs ++ [a]) → 
  ins_list x (xs ++ a :: ys) =
  (if x < a then ins_list x xs ++ (a :: ys) else xs ++ ins_list x (a :: ys)) :=
assume h_xsa,

classical.by_cases 
  (assume x_le_a : x < a,
   match xs, h_xsa with
   | [], _       := by simp [x_le_a, ins_list]
   | (hd::tl), l := trichotomy_on x hd
      (assume h_ineq, by simp [x_le_a, h_ineq, ins_list])
      (assume h_ineq, by simp [x_le_a, h_ineq, ins_list])
      (assume h_ineq, 
        have ih :_ := by apply _match tl  (sorted_wrt_cons l), 
        by simpa [x_le_a, h_ineq, ins_list] using ih)
   end)

  (assume not_x_le_a : ¬ x < a, 
   match xs, h_xsa with
   | [], _    := by simp [not_x_le_a, ins_list]
   | (hd :: tl), l  := 
      have ¬ x = hd ∧ ¬ x < hd := sorted_mk_ineq l not_x_le_a,
      have ih :_  := by apply _match tl (sorted_wrt_cons l),
        by simpa [not_x_le_a, this, ins_list] using ih
   end)

-- derivations of the main insert lemma
@[ins_list_simps]
lemma ins_list_sorted1 (ys : list α) : sorted (xs ++ [a])  →  a ≤ x →
  ins_list x (xs ++ a :: ys) = xs ++ ins_list x (a :: ys) :=
assume h_xsa a_leq_x,
by simp [not_lt_of_ge a_leq_x, ins_list_sorted h_xsa]

@[ins_list_simps]
lemma ins_list_sorted2 (ys : list α) : sorted (xs ++ [a]) → x < a → 
  ins_list x (xs ++ a :: ys) = ins_list x xs ++ (a :: ys) :=
assume h_xsa x_le_a,
by simp [x_le_a, ins_list_sorted h_xsa]

end insert_lemmas

section delete_lemmas

variables {α : Type} [decidable_linear_order α]
variables {x a b c d: α}
variables {xs ys zs us vs: list α}

lemma del_list_idem: x ∉ xs → del_list x xs = xs :=
assume x_not_in_xs,
match xs, x_not_in_xs with
| [], _          := by simp [del_list]
| (hd :: tl), h' := 
  begin
    simp [decidable.not_or_iff_and_not] at h',
    simp [del_list, h'],
    exact _match tl h'.right
  end
end

lemma sorted_del_list: sorted xs → sorted (del_list x xs) :=
assume hxs,
match xs, hxs with 
| [], _   := by simp [del_list]; exact cnil
| [hd], _ := by by_cases h : x = hd;
    { simp [del_list, h], exact cnil <|> exact csingleton  }
| (x₁ :: x₂ :: tl), (ccons r l) := 
let x2t := x₂ :: tl in
  begin
    by_cases x_eq_x1 : x = x₁,
      
      { simpa [del_list, x_eq_x1] },
      
      {
        rename x_eq_x1 x_neq_x1, 
        simp [del_list, x_neq_x1],
        -- we need to check one more element because of sorted
        by_cases x_eq_x2 : x = x₂,
      
        { simp [del_list, x_eq_x2], solve_by_elim with sorted_lems},
        { 
          rename x_eq_x2 x_neq_x2, 
          simp [x_neq_x2],

          have : sorted (del_list x x2t) := by solve_by_elim,
          apply ccons r ,
          simpa [del_list, x_neq_x2] using this}
      }
  end
end


lemma del_list_sorted: sorted (xs ++ a :: ys) → 
  del_list x (xs ++ a :: ys) = (if x < a then del_list x xs ++ a :: ys 
                                         else xs ++ del_list x (a :: ys)) :=
assume hxs,
classical.by_cases 
  (assume x_lt_a : x < a,
    -- note that x can only be member of xs
    match xs, hxs with
    | [], _ := -- as xs is empty, x ∉ xs ++ a :: ys, so del_list x (xs ++ a :: ys) = xs ++ a :: ys
              begin 
                simp [x_lt_a, del_list, ne_of_lt x_lt_a], 
                refine del_list_idem _,
                exact sorted_not_in2 hxs x_lt_a
               end
    | (hd :: tl), l := have l' : sorted (tl ++ a :: ys) := sorted_wrt_cons l,
    begin
      simp [x_lt_a, del_list],
      by_cases x_eq_hd : x = hd,
      { simp [x_eq_hd] },
      { rename x_eq_hd x_neq_hd, 
        have h :_ := _match tl l',
        simp [x_neq_hd], apply eq.trans h, simp [x_lt_a] }
    end
  end)

  (assume not_x_lt_a : ¬ x < a, 
    -- x can only be member of a :: ys, not xs
    match xs, hxs with
    | [], _ := begin simp [not_x_lt_a, del_list] end
    | (hd :: tl), l := 
      have ¬ x = hd ∧ ¬ x < hd := sorted_mk_ineq l not_x_lt_a,
      begin
      simp [del_list, not_x_lt_a, this],
      by_cases h : (x = a),
      {
        have ih:_ := _match tl (sorted_wrt_cons l),
        rw h at ih, simp [h], rw ih,
        simp [lt_irrefl, del_list]
      },
      {
        have ih:_ := _match tl (sorted_wrt_cons l),
        simp [not_x_lt_a, del_list] at ih,
        rw ih,
        simp [not_x_lt_a, del_list, h]
      }
      end
  end)

@[del_list_simps]
lemma del_list_sorted1: sorted (xs ++ a :: ys) → a ≤ x → 
  del_list x (xs ++ a :: ys) = xs ++ del_list x (a :: ys) :=
assume h a_leq_x, by simp [not_lt_of_ge a_leq_x, del_list_sorted h]

@[del_list_simps]
lemma del_list_sorted2: sorted (xs ++ a :: ys) → x < a →
  del_list x (xs ++ a :: ys) = del_list x xs ++ a :: ys :=
assume h x_lt_a, by simp [x_lt_a, del_list_sorted h]

@[del_list_simps]
lemma del_list_sorted3:
  sorted (xs ++ a :: ys ++ b :: zs) →  x < b → 
  del_list x (xs ++ a :: ys ++ b :: zs) = del_list x (xs ++ a :: ys) ++ b :: zs :=
begin
 generalize hl : xs ++ a :: ys = l,
 intros h x_lt_b,
 simp [x_lt_b, del_list_sorted h]
end

@[del_list_simps]
lemma del_list_sorted3':
  sorted (xs ++ a :: ys ++ b :: zs) →  b ≤ x → 
  del_list x (xs ++ a :: ys ++ b :: zs) = xs ++ a :: ys ++ del_list x (b :: zs) :=
begin
 generalize hl : xs ++ a :: ys = l,
 intros h b_leq_x,
 simp [not_lt_of_ge b_leq_x, del_list_sorted h]
end

@[del_list_simps]
lemma del_list_sorted4:
  sorted (xs ++ a :: ys ++ b :: zs ++ c :: us) → x < c → 
  del_list x (xs ++ a :: ys ++ b :: zs ++ c :: us) = 
  del_list x (xs ++ a :: ys ++ b :: zs) ++ c :: us :=
begin
 generalize hl : xs ++ a :: ys ++ b :: zs = l,
 intros h x_lt_c,
 simp [x_lt_c, del_list_sorted h]
end

@[del_list_simps]
lemma del_list_sorted5:
  sorted (xs ++ a :: ys ++ b :: zs ++ c :: us ++ d :: vs) →  x < d → 
   del_list x (xs ++ a :: ys ++ b :: zs ++ c :: us ++ d :: vs) =
   del_list x (xs ++ a :: ys ++ b :: zs ++ c :: us) ++ d :: vs :=
begin
 generalize hl : xs ++ a :: ys ++ b :: zs ++ c :: us = l,
 intros h x_le_d,
 simp [x_le_d, del_list_sorted h]
end

end delete_lemmas