import tactic

namespace tactic
namespace interactive
open interactive

meta def apply_assumptions_and_simp 
  (hs :   parse simp_arg_list) 
  (attr_names : parse types.with_ident_list)
  (tgt : parse $ optional (lean.parser.tk "using" *> types.texpr)) : tactic unit := 
  apply_assumption local_context $
    all_goals (simpa none ff hs [] none)

meta def clear_finish (hs : interactive.parse simp_arg_list) (cfg : auto.auto_config := {}) : tactic unit :=
  clear_ >> finish hs cfg

end interactive
end tactic

section arith_aux

lemma one_eq_one_plus { x : ℕ }: 1 = 1 + x → 0 = x := 
begin
  intro h,
  replace h := eq.trans (add_zero 1) h,
  replace h := add_left_cancel h,
  assumption
end 

lemma transferK_to_right { k x y: ℕ }: k + x = y → x = y - k :=
begin
  intro h,
  replace h := congr_arg (λ x, x - k) h,
  simpa [nat.add_sub_cancel_left] using h
end 

lemma not_k_plus_one_eq_k { k x : ℕ } : k + (1 + x) = k → false :=
begin
  intro h,
  replace h := transferK_to_right h,
  simpa [nat.sub_self] using h
end

end arith_aux

namespace utils

variable {α : Type}

lemma not_z_eq_x_and_not_z_le_x { α : Type} [decidable_linear_order α] { x y z : α } 
( h₁ : ¬ z < y) (h₂ : x < y) : ¬ z = x ∧ ¬ z < x :=
begin 
  intros,
  cases eq_or_lt_of_not_lt h₁;
  rw ←h at h₂ <|> replace h₂ := lt_trans h₂ h;
  exact ⟨ (ne_of_lt h₂).symm, lt_asymm h₂ ⟩ 
end


def trichotomy_on {Q : Prop} [decidable_linear_order α] (x y : α)
(h₁ : (x < y) → Q)
(h₂ : ¬ (y < y) ∧ (x = y)  → Q)
(h₃ : ¬ (x < y) ∧ (x ≠ y) ∧ (y < x) → Q) : Q :=
classical.by_cases 
    (assume a: x < y, h₁ a) 
    
    (assume a : ¬ (x < y),  
     have x = y ∨ y < x := eq_or_lt_of_not_lt a,
     or.elim this 
       (assume b, h₂ ⟨ lt_irrefl y, b⟩)
       (assume b, h₃ ⟨a, (ne_of_lt b).symm,  b⟩))


lemma cancel_append {xs ys zs: list α} : xs ++ ys = xs ++ zs ↔ ys = zs :=
  by induction xs; finish

end utils

