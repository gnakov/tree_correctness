import .tree
import .operations
import ..list_operations

namespace tree
namespace lemmas

variables {α : Type} [decidable_linear_order α]
variables {x a : α}
variables {t l r t' : tree α}

private lemma sorted_node_collapse : 
  sorted (inorder (node l x r)) → sorted (inorder l) ∧ sorted (inorder r) := 
begin
  intro h,
  simp [inorder] at h,
  have st_l :_  := sorted_wrt_append (sorted_mid_iff.mp h).left,
  have st_r :_  := sorted_wrt_cons (sorted_mid_iff.mp h).right,
  exact ⟨st_l, st_r⟩
end

theorem isin: sorted (inorder t) → (mem x t ↔ x ∈ inorder t) :=
begin
  intro hst,
  -- both directions are pretty similiar, so we do them together
  apply iff.intro;
  { intros,
    induction t,
    { simpa [mem] using a },
    { simp [mem, inorder] at a ⊢, 
      --get the sorted subtrees t_l and t_r
      have st_l_and_s_tr := sorted_node_collapse hst,
      simp [inorder] at hst,
      
      rcases (lt_trichotomy x t_a) with x_lt_ta | x_eq_ta | ta_lt_x;
      -- case when x = t_a, it is directly a disjunct in our goal, so nothing to do
      { finish }
      -- case when x < t_a, 
      -- by assumption we have that x ∈ inorder t_l ∨ x = t_a ∨ x ∈ inorder t_r
      -- show that x ∉ tree.inorder t_r and as x ≠ t_a, we have that x ∈ inorder t_l
      -- and we can apply the IH
      <|> { have x_nin_tr :_ := sorted_not_in2 hst x_lt_ta, finish [lt_irrefl] }
      -- case when t_a < x, similarly show that x ∉ inorder t_l
      <|> { have x_nin_tl :_ := sorted_not_in3 hst ta_lt_x, finish [le_of_lt] }
    }
  }
end

theorem inorder_insert:
  sorted (inorder t) → inorder (insert x t) = ins_list x (inorder t) :=
begin
  intro hst,
  induction t,
  
  {simp [inorder, insert, ins_list]},

  { -- t = node (t_l t_a t_r)
    simp [inorder, insert, ins_list] at ⊢ t_ih_l t_ih_r,
    -- get the sorted subtrees
    have st_l_and_s_tr := sorted_node_collapse hst,
    -- expand the inorder of t to the concatenation of the inorder-ed subtrees
    simp [inorder] at hst,
    
    rcases (lt_trichotomy x t_a) with x_lt_ta | x_eq_ta | ta_lt_x,
    {
      --apply IH for the left subtree t_l
      simp [x_lt_ta, inorder],
      specialize t_ih_l st_l_and_s_tr.left,
      simp [t_ih_l],
      -- get the shape right so we can apply lemma ins_list_sorted2,
      apply eq.symm, 
      apply ins_list_sorted2;
      clear_finish [sorted_mid_iff.mp hst]
    },
    {
      -- x = t_a, no need to invoke any IH, just lemma ins_list_sorted1
      simp [x_eq_ta, lt_irrefl, inorder, ins_list],
      have h  := ins_list_sorted1 (tree.inorder t_r)
                   (sorted_mid_iff.mp hst).left (le_of_eq x_eq_ta.symm),
      subst_vars,
      simp [h, ins_list, lt_irrefl]
    },
    { 
      -- get the inequalities in a nicer way for expandin in if defs.
      have ineq : ¬ x = t_a ∧ ¬ x < t_a := 
                  ⟨(ne_of_lt ta_lt_x).symm, lt_asymm ta_lt_x⟩,
      
      have h  := ins_list_sorted1 (tree.inorder t_r)
                  (sorted_mid_iff.mp hst).left (le_of_lt ta_lt_x),
      
      -- apply the IH for t_r and rewrite the result of ins_list_sorted1
      specialize t_ih_r st_l_and_s_tr.right,
      simp [ins_list] at h,
      simp [ineq, inorder, t_ih_r, h, ins_list]
    }
  }
end

private lemma split_minD: 
  (t = node l a r) → tree.split_min l a r = (x, t') → x :: inorder t' = inorder t :=
begin
  intros t_eq_lar split_lar_eq_xt',
  
  induction t generalizing l a r t',
  
  -- t is leaf
  case leaf : { contradiction },
  
  case node : {

    cases l_eq : l,
    case leaf : { 
      -- t has shape ⟨leaf, a, r⟩, l = t_l = leaf
      simp [l_eq, split_min] at split_lar_eq_xt',
      finish },
    case node :{
      -- t := ⟨ l@(node l_1 a_1 r_1), a, r⟩ 
      
      -- we need lean to see that t' is not just an arbitrary tree, 
      -- but the result of split_min t
      cases t',
      case leaf : {
        -- contradiction as split_min extracts the  tree without the leftmost node,
        -- and there exists a non-leaf node under t, namely l
        simp [l_eq, split_min] at split_lar_eq_xt',
        contradiction
      },
      case node : {
        -- the case where split_min l(= l_1 a_1 r_1) returns a subtree 
        simp at t_eq_lar,
        simp [t_eq_lar] at t_ih_l,

        -- get that (x, t') = (split_min l)
        simp [l_eq, split_min] at split_lar_eq_xt',
        rcases split_lar_eq_xt' with ⟨split_fst, split_snd, var_eq⟩,
        have : split_min l_1 a_1 r_1 = (x, t'_l) := 
                by apply prod.eq_iff_fst_eq_snd_eq.mpr; finish,
        
        -- apply the IH for the left subtree of t, namely l
        specialize t_ih_l l_eq this,
  
        simp [t_eq_lar, l_eq, inorder] at t_ih_l ⊢,
        rw   ←list.cons_append,
        simp [t_ih_l, var_eq],
      }
    }
  }
end

theorem inorder_delete: 
  sorted (inorder t) → inorder (delete x t) = del_list x (inorder t) :=
begin
  intro hsit,
  induction t,
  { -- t == leaf
    simp [inorder, delete, del_list] 
  },

  { /- t is   (val : t_a)
                  / \
               t_l   t_r    -/
    simp [inorder, delete, del_list] at *,
    split_ifs,
    { -- x < t_a, prove with the ind. hypothesis on t_l
      simp [inorder, delete],
      rw (del_list_sorted2 hsit h),
      
      have : sorted (inorder t_l) := 
        sorted_wrt_append (sorted_mid_iff.mp hsit).left,
      
      finish 
    },
    { -- x = a, so we have to delete the (node t_l t_a t_r)
      simp [inorder, delete, h_1],

      cases h_t_r : t_r,
      { /- t is   (val : t_a (=x))
                      / \
                   t_l   leaf   -/
        subst_vars,
        simp [delete],
        rw (del_list_sorted1 hsit (le_refl t_a)),
        simp [del_list, inorder]
      },
      { 
        simp [delete, inorder],
        cases h_l : l,
        {/- t is  (val : t_a (=x))
                      / \
                   t_l   (val : a)  
                             / \  
                         leaf   r  -/
          simp [split_min, delete, inorder, del_list],
          
          -- before aplying lemma del_list_sorted1, we need to expand to actual state of t_r
          simp [inorder, h_t_r, h_l] at hsit,
          rw (del_list_sorted1 hsit (le_refl t_a)),
          simp [del_list]
        },
        
        {/- t is  (val : t_a (=x))
                      / \
                   t_l   (val : a)  
                           /   \  
                  (val : a_1)   r  
                      / \ 
                   l_1   r_1       -/
        simp [split_min, delete, inorder, del_list],
        

        -- split_lm applies the lemma to the l-subtree to basically get x' :: inorder' t = inorder l,
        -- where (x', t') := split_min l
        have split_lm := split_minD h_l
                        (show split_min l_1 a_1 r_1 = 
                          ((split_min l_1 a_1 r_1).fst, (split_min l_1 a_1 r_1).snd),
                        by simp),
        
        -- busy work so we can rewrite the result in the goal
        rw ←list.cons_append,
        simp [split_lm],

        -- now we can apply lemma del_list_sorted1, but first we need to expand the subtree t_r
        -- in the sorted assumption
        simp [h_t_r, h_l, inorder] at hsit,
        rw (del_list_sorted1 hsit (le_refl t_a)),
        
        simp [del_list, h_l, inorder],
        }
      }
    },
    { -- x is in the right subtree, analogous to the left variant, but uses del_list_sorted1
      simp [inorder, delete],
      rw (del_list_sorted1 hsit (le_of_not_lt h)),
      
      have : sorted (inorder t_r) :=  sorted_wrt_cons (sorted_mid_iff.mp hsit).right,
  
      finish [del_list]
    }
  }
end

end lemmas
end tree