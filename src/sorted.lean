import utils

section chained

parameter {α : Type} 
parameter {rel : α → α → Prop}

variables {x y: α} 
variables {xs ys: list α}

-- model a chain of the shape x₁ R x₂ R... xₙ, s.t for all 1 ≤ i < n,  (xᵢ R xᵢ₊₁) holds
inductive chained : list α  → Prop
| nil {} : chained []
| singleton { x : α } : chained [x]
| cons {x y : α} {xs : list α} (h : rel x y) (yxs : chained (y :: xs)) :
    chained ( x :: y :: xs)

export chained (renaming nil → cnil) (renaming cons → ccons) (renaming singleton → csingleton)
 
lemma chained_wrt_cons : chained (x :: xs) → chained xs :=
assume h,
match xs, h with 
| [], _        := cnil
| (_ :: _), ih := by cases ih; assumption
end 

lemma chained_wrt_append : chained (xs ++ [x]) → chained xs :=
assume h,
match xs, h with
| [] , _   := cnil
| [_], _   := csingleton
| (x₁ :: x₂ :: t), (ccons r12 _) :=
    have chained (x₂::t) := by solve_by_elim,
    ccons r12 this
end

lemma chained_mid_iff: 
    chained (xs ++ y :: ys) ↔ (chained (xs ++ [y]) ∧ chained (y :: ys)) :=
iff.intro 
    (assume h, match xs, h with
    | [],      yys                     := ⟨csingleton, yys⟩
    | [_],     (ccons r yys)           := ⟨ccons r $ csingleton, yys⟩
    | (x₁ :: x₂ :: t), (ccons r x2yys) := 
        have chained (x₂ :: t ++ [y]) ∧ chained (y :: ys) := by apply _match (x₂ :: t) x2yys,
        ⟨ccons r this.left, this.right ⟩
    end)

    (assume ⟨ h_xsy, h_yys⟩, match xs, h_xsy, h_yys with
    | [], _, yys := yys
    | [_], (ccons r _), yys  := ccons r yys
    | (x₁ :: x₂ :: t), (ccons r x2ty), g  := 
        have chained (x₂ :: t ++ y :: ys) := by solve_by_elim,
        ccons r this
    end)


section trans_rel_chain

parameter [decidable_rel rel]
parameter [is_trans α rel]

lemma trans_chained_mid_iff : chained (x :: xs ++ y :: ys) ↔
  (chained (x :: xs) ∧ rel x y ∧ chained (xs ++ [y]) ∧ chained (y :: ys)) :=
iff.intro
    (assume h_l, 
    match xs, ys, h_l with
    | [],  _, (ccons r l) := ⟨ csingleton, r, csingleton, l ⟩ 
    | [_], _, (ccons r $ ccons r' yt) := ⟨ ccons r  $ csingleton, trans r r', 
                                            ccons r' $ csingleton, yt ⟩
    | (x₁ :: x2t@(x₂ :: t)), l, (ccons r (ccons r' l')) :=
        let  xl'  := ccons (trans r r') l' in
        have chained (x :: x2t) ∧ rel x y ∧ chained (x2t ++ [y]) ∧ chained (y :: l)
            := by solve_by_elim,
        let ⟨a, b, c, d ⟩ := this in
            ⟨ ccons r $ ccons r' $ chained_wrt_cons a, b, ccons r' c, d ⟩ 
    end)
    (assume ⟨a, b, c, d⟩,
    match xs, a, c with
    | [], _, _  := ccons b d
    | [h], (ccons r _), (ccons r' _) := ccons r $ ccons r' d
    | (x₁ :: x2t@ (x₂ :: t)), (ccons r $ ccons r' l), (ccons _ l') :=
        let  xl  := ccons (trans r r') l in
        have chained (x :: x2t ++ y :: ys) := by solve_by_elim,
        ccons r $ ccons r' $ chained_wrt_cons this
    end)


lemma trans_chained_cons_iff: chained (x :: xs) ↔ ((∀y ∈ xs, rel x y) ∧ chained xs) :=
iff.intro
    (assume h_xxs,
    match xs, h_xxs with
    | [], x            := ⟨ (λy h, by simpa using h), cnil⟩
    | [_], (ccons r _) := ⟨ (λy h, by simp at h; rwa ←h at r), csingleton ⟩ 
    | (x₁ :: x₂ :: t), (ccons r l@(ccons r' sx2t)):= 
        have sxx2t : _    := ccons (trans r r') sx2t,
        have ∀ (y : α), y ∈ x₂ :: t → rel x y  
                := and.left $ by solve_by_elim,
        ⟨ (λ y h, by clear_finish), l⟩
    end)
    (assume ⟨f, h_xs⟩,
    match xs, h_xs, f with
    | [], _, _        := csingleton
    | (h :: _), l, f' := ccons (f' h $ by simp) l
    end)

lemma trans_chained_snoc_iff: chained (xs ++ [x]) ↔ (chained xs ∧ (∀y ∈ xs, rel y x)) :=
iff.intro
    (assume h_xsx,
    and.intro (chained_wrt_append h_xsx) $
    assume y h_y_in_xs,
    match xs, h_xsx, h_y_in_xs with
    | [], _ , h'           := by simpa using h'
    | [a], ccons r t , h'  := by simp at h'; rwa ←h' at r
    | (x₁ :: x₂ :: t), (ccons r x2tx), (or.inr h') := by apply _match (x₂ :: t) x2tx h'
    | (x₁ :: x₂ :: t), (ccons r x2tx), (or.inl h') := 
        have f :_            := (trans_chained_cons_iff.mp x2tx).left,
        have ryx2 : rel y x₂ := by rwa ←h' at r,
        have x ∈ [x]         := by simp,
        trans ryx2 $ f x $ list.mem_append_right _ this
    end)
    
    (assume ⟨h_xs, f⟩,
    match xs, h_xs, f with
    | [], _, _   := csingleton
    | [a], _, f' := ccons (f' a (by simp)) $ csingleton
    | (x₁ :: x₂ ::t) , (ccons r l) , f' := ccons r $ 
        by { refine _match (x₂ :: t) l _, clear_finish }
    end)


lemma chained_drop_2nd: chained (x :: y :: ys) → chained (x :: ys) :=
assume xyys,
match ys , xyys with 
| [], _ := csingleton
| (hd :: _), (ccons r $ ccons r' l) := ccons (trans r r') l
end

lemma chained_replace_1st: chained (y :: ys) → rel x y → chained (x :: ys) :=
assume yys r, chained_drop_2nd $ ccons r yys
 
end trans_rel_chain

end chained

---------- 

namespace simp_attr

@[user_attribute]
meta def sorted_lems : user_attribute := { name := `sorted_lems, descr := "All the lemmas for 'sorted'" }

end simp_attr

section sorted_linear_order

open chained
variables {α : Type} [decidable_linear_order α]
variables {x y: α} {xs ys: list α}


def sorted : list α → Prop := @chained α (<)

@[sorted_lems] 
lemma sorted_wrt_cons : sorted (x :: xs) → sorted xs :=
chained_wrt_cons 

@[sorted_lems]
lemma sorted_wrt_append : sorted (xs ++ [x]) → sorted xs :=
chained_wrt_append

@[sorted_lems]
lemma sorted_mid_iff : sorted (xs ++ y :: ys) ↔ (sorted (xs ++ [y]) ∧ sorted (y :: ys)) :=
chained_mid_iff 

@[sorted_lems]
lemma sorted_mid_iff2 :  sorted (x :: xs ++ y :: ys) ↔
    (sorted (x :: xs) ∧ x < y ∧ sorted (xs ++ [y]) ∧ sorted (y :: ys)) :=
trans_chained_mid_iff

lemma sorted_cons_iff : sorted (x :: xs) ↔ ((∀y ∈ xs, x < y) ∧ sorted xs) :=
trans_chained_cons_iff

lemma sorted_snoc_iff : sorted (xs ++ [x]) ↔ (sorted xs ∧ (∀y ∈ xs, y < x)) :=
trans_chained_snoc_iff

@[sorted_lems]
lemma sorted_drop_2nd : sorted (x :: y :: ys) → sorted (x :: ys) :=
chained_drop_2nd

@[sorted_lems]
lemma sorted_replace_1st : sorted (y :: ys) → x < y → sorted (x :: ys) :=
chained_replace_1st

@[sorted_lems]
lemma sorted_not_in1: sorted (y :: ys) → x < y → x ∉ ys :=
assume yys r,
match ys, yys with
| [], _ := by clear_finish
| (hd :: tl), (ccons r' l) :=
have l' :_   := sorted_drop_2nd $ ccons r' l,
have x ≠ hd  := ne_of_lt $ trans r r', 
    by simp [decidable.not_or_iff_and_not]; refine and.intro this _;
       solve_by_elim
end

@[sorted_lems]
lemma sorted_not_in2: sorted (xs ++ y :: ys) → x < y → x ∉ ys :=
assume xyys, sorted_not_in1 (sorted_mid_iff.mp xyys).right

@[sorted_lems]
lemma sorted_not_in3 {z : α}: sorted (xs ++ y :: ys) → y < z → z ∉ xs :=
assume xyys h,
match xs, xyys with
| [], _ := by clear_finish
| (hd :: tl), l := 
have l' :_   := sorted_wrt_cons l,
have hd < y  := (sorted_mid_iff2.mp l).right.left,
have z ≠ hd  := (ne_of_lt $ trans this h).symm,
by simp [decidable.not_or_iff_and_not]; refine and.intro this _;
   solve_by_elim 
end

lemma sorted_mk_ineq {a : α} : 
    sorted (x :: xs ++ y :: ys) → ¬ a < y → ¬ a = x ∧ ¬ a < x :=
assume sl h,
have x < y := (sorted_mid_iff2.mp sl).right.left,
utils.not_z_eq_x_and_not_z_le_x h this


end sorted_linear_order
