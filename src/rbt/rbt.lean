namespace rbt

variable {α : Type}

inductive color : Type 
| red   : color 
| black : color

export color (red black)

inductive rbt (α : Type) : Type
| leaf {} : rbt
| node (l : rbt) (a : α) (c : color) (r : rbt) : rbt

export rbt.rbt (leaf node)

variables (l : rbt α) (a : α) (r : rbt α)

@[pattern, reducible]
def R : rbt α := node l a red r
@[pattern, reducible]
def B : rbt α := node l a black r

end rbt

