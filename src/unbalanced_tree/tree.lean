namespace tree

inductive tree (α : Type) : Type
| leaf {} : tree
| node (l : tree) (a : α) (r : tree) : tree

export tree.tree (leaf node)

end tree