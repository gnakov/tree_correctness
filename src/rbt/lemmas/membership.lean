import ..operations
import ...list_operations

namespace rbt
namespace lemmas

variables {α : Type} [decidable_linear_order α]
variable  {t : rbt α}
variable  {x : α}

theorem isin: sorted (inorder t) → (mem x t ↔ (x ∈ inorder t)) :=
begin
  intro hst,
  apply iff.intro;
  { intros,
    induction t,
    { simpa [mem] using a },
    { simp [mem, inorder] at a ⊢ hst,
      have s_t_l : sorted (inorder t_l) := sorted_wrt_append (sorted_mid_iff.mp hst).left,
      have s_t_r : sorted (inorder t_r) := sorted_wrt_cons (sorted_mid_iff.mp hst).right,

      rcases (lt_trichotomy x t_a) with x_lt_ta | x_eq_ta | ta_lt_x;
      { finish }
      <|> { have x_nin_tr :_ := sorted_not_in2 hst x_lt_ta, finish [lt_irrefl] }
      <|> { have x_nin_tl :_ := sorted_not_in3 hst ta_lt_x, finish [le_of_lt] }
    }
  }
end

end lemmas
end rbt