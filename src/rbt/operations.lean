import .rbt

namespace rbt

variables {α : Type} [decidable_linear_order α]

section coloring

def paint : color → rbt α → rbt α
| _ leaf := leaf
| c (node l a _ r) := node l a c r

def get_color : rbt α → color 
| leaf            := black
| (node _ _ c _)  := c

end coloring
---------------------------------------------------------------------------------------------------
section rebalancing

def baliL : rbt α → α → rbt α → rbt α
| (R (R t₁ a₁ t₂) a₂ t₃) a₃ t₄ := R (B t₁ a₁ t₂) a₂ (B t₃ a₃ t₄)
| (R t₁ a₁ (R t₂ a₂ t₃)) a₃ t₄ := R (B t₁ a₁ t₂) a₂ (B t₃ a₃ t₄)
| t₁ a t₂                      := B t₁ a t₂

def baliR : rbt α → α → rbt α → rbt α
| t₁ a₁ (R t₂ a₂ (R t₃ a₃ t₄)) := R (B t₁ a₁ t₂) a₂ (B t₃ a₃ t₄)
| t₁ a₁ (R (R t₂ a₂ t₃) a₃ t₄) := R (B t₁ a₁ t₂) a₂ (B t₃ a₃ t₄)
| t₁ a t₂ := B t₁ a t₂

def baldL : rbt α → α → rbt α → rbt α
| (R t₁ x t₂) y t₃          := R (B t₁ x t₂) y t₃
| bl x (B t₁ y t₂)          := baliR bl x (R t₁ y t₂)
| bl x (R (B t₁ y t₂) z t₃) := R (B bl x t₁) y (baliR t₂ z (paint red t₃))
| t₁ x t₂                   := R t₁ x t₂

def baldR : rbt α → α → rbt α → rbt α
| t₁ x (R t₂ y t₃)          := R t₁ x (B t₂ y t₃)
| (B t₁ x t₂) y t₃          := baliL (R t₁ x t₂) y t₃
| (R t₁ x (B t₂ y t₃)) z t₄ := R (baliL (paint red t₁) x t₂) y (B t₃ z t₄)
| t₁ x t₂                   := R t₁ x t₂

def combine : rbt α → rbt α → rbt α
| leaf t := t
| t leaf := t 
| (R t₁ a t₂) (R t₃ c t₄) :=
  match combine t₂ t₃ with 
    | R t₂' b t₃' := (R (R t₁ a t₂') b (R t₃' c t₄)) 
    | t₂₃         := R t₁ a (R t₂₃ c t₄)
  end
| (B t₁ a t₂) (B t₃ c t₄) := 
  match combine t₂ t₃ with
    | R t₂' b t₃' := R (B t₁ a t₂') b (B t₃' c t₄) 
    | t₂₃         := baldL t₁ a (B t₂₃ c t₄)
  end
| t₁ (R t₂ a t₃) := R (combine t₁ t₂) a t₃
| (R t₁ a t₂) t₃ := R t₁ a (combine t₂ t₃) 

end rebalancing
---------------------------------------------------------------------------------------------------
section operations

def inorder : rbt α → list α 
| leaf := []
| (node l a _ r) := inorder l ++ a :: inorder r

def mem (x : α) : rbt α → Prop
| leaf            := false
| (node l a _ r)  := if (x < a) then mem l
                     else if (x = a) then true
                     else mem r

def ins (x : α) : rbt α → rbt α 
| leaf        := R leaf x leaf
| t@(B l a r) := if x < a then baliL (ins l) a r
                 else if x = a then t
                 else baliR l a (ins r)
| t@(R l a r) := if x < a then R (ins l) a r
                 else if x = a then t
                 else R l a (ins r)

def insert (x : α): rbt α → rbt α  
| t := paint black $ ins x t

def del (x : α): rbt α → rbt α 
| leaf           := leaf
| (node l a _ r) := 
      if x < a then 
        match l with 
        | (B _ _ _) := baldL (del l) a r
        | _         := R (del l) a r
        end
      else if x = a then combine l r
      else 
        match r with 
        | (B _ _ _) := baldR l a (del r)
        | _         := R l a (del r)
        end

def delete (x : α): rbt α → rbt α 
| t := paint black $ del x t

end operations
---------------------------------------------------------------------------------------------------
section invariants

def bheight : rbt α → ℕ
| leaf        := 0
| (B l _ _)   := bheight l + 1
| (R l _ _)   := bheight l 

def invc : rbt α → Prop
| leaf            := true
| (node l _ c r)  := invc l ∧ invc r ∧ (c = red → get_color l = black ∧ get_color r = black)

@[reducible]
def invc2 : rbt α → Prop  -- ‹Weaker version›
| leaf            := true
| (node l _ _ r)  := invc l ∧ invc r

def invh : rbt α → Prop
| leaf            := true
| (node l _ _ r)  := invh l ∧ invh r ∧ bheight l = bheight r

@[reducible]
def inv : rbt α → Prop
| t := invc t ∧ invh t ∧ get_color t = black 

end invariants

end rbt